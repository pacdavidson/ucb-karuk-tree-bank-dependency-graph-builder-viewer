$(document).ready(function(){

		      // ------------------------------
		      //      data and its manipulation
		      // ------------------------------

		      var karukTexts = {},                                                            // object to store data as it gets worked on
		      UIDs = {},		                                                      // an object of arrays to store unique ids
		      synRels = ["PRED", "TAM", "NEG", "SBJ", "OBJ", "IO", "POSS", "ATR", "COMP", 
				 "QUOT", "ADV", "SPRED", "APOS", "SUB", "COORD"],                     // a placeholder
		      pos = ["N", "V", "ADJ", "ADV", "DET", "IDEO", "INTERJ", "NUM", "PCL", "PRON"],  // ditto
		      cliticLemmas = {                                                                // ditto
			  proclitics: ["4407", "4408", "5082", "5506", "7291"],
			  enclitics: ["1402", "1670", "1729", "6381", "6383"]
		      },
		      allCliticLemmas = cliticLemmas.proclitics.concat(cliticLemmas.enclitics);                                                         

		      var textIndexList = function(xml){
			  var textIndex = [];
			  $(xml).find("text").each(function() {
						       textIndex.push($(this).attr("id"));
						   });
			  return textIndex;
		      };


		      var karukText = function (xmlData) {
			  var xmldata = $(xmlData); // convert into a jquery object
			  var textNode = xmldata.find("text");
			  this.filename = textNode.attr("lemma") ? textNode.attr("lemma") : textNode.attr("id");
			  //console.log(this.filename);
			  if (xmldata.find("kar")){   // if there are <kar> in xml	
			      this.updateXmlNaming(xmldata);
			  }
			  this.getExistingUIDs(xmldata, this.filename);
			  this.assignHeads(xmldata, this.filename);
			  this.headsInText = getParagraphsInText(xmldata);
			  this.text = xmldata.find("text").prop('outerHTML'); 
			  return this;
		      };


		      karukText.prototype.updateXmlNaming = function (xmldataIn){
			  xmldataIn.find("kar").each(function(){
							 var descendants = $(this).html();
							 $(this).replaceWith('<txt lang="kyh">' + descendants + '</txt>');
						     });
			  xmldataIn.find("eng").each(function(){
							 var descendants = $(this).html();
							 $(this).replaceWith('<trans lang="eng">' + descendants + '</trans>');
						     });
			  xmldataIn.find("m").each(function(){
						       var idNum = $(this).attr("id");
						       if (idNum) {
							   $(this).removeAttr("id").attr("lemma", idNum);
						       }
						       var lang = $(this).attr("language");
						       if (lang) {
							   if (lang === "english"){
							       $(this).removeAttr("language").attr("lang", "eng");
							   }
						       }
						   });	
		      };

		      karukText.prototype.getExistingUIDs = function (xmldataIn, fileName) {
			  var extantUIDs = [];
			  var sentences = xmldataIn.find("data");
			  sentences.find("*[id]").each(function(){
							   var re = new RegExp(fileName + ":");
							   if (re.test($(this).attr("id"))) {
							       var hivedOut = $(this).attr("id").replace(re, "");
							       var hivedOutAsNum = parseInt(hivedOut, 10);
							       extantUIDs.push(hivedOutAsNum);
							   }
						       });
			  UIDs[fileName] = extantUIDs;
		      };

		      karukText.prototype.assignHeads = function (xmldataIn, fileName){
			  xmldataIn.find("w").each(function(){
						       assignUID($(this), fileName);
						       /*
							- select morphemes. if more than one:
							- only clitics? skip assignment of last
							- if last m 1418 or 6175, if preceded by clitic 5506, don't assign to 5506 either 
							*/
						       var morphemes = $(this).children();
						       var morphemesLen = morphemes.length;
						       if (morphemesLen > 1){ // don't assign an ID to any morpheme in a one morpheme word
							   var lastMorpheme = morphemesLen - 1;
							   var lastMorphemeLemma = $(morphemes[lastMorpheme]).attr("lemma");
							   var penultMorphemeLemma = $(morphemes[lastMorpheme - 1]).attr("lemma");
							   var skipPenultClitic = function(){
							       if ((lastMorphemeLemma === "1418" || lastMorphemeLemma === "6175") && penultMorphemeLemma === "5506"){
								   return true;
							       } else {
								   return false;
							       }    
							   }();
							   var isClitic = function(morph){
							       for (var i = 0, j = allCliticLemmas.length; i < j; i++){
								   if ($(morph).attr("lemma") === allCliticLemmas[i]){
								       //console.log($(morph).attr("lemma") + " is a clitic");
								       return true;
								   } 
							       }
							       return false;
							   };
							   var onlyClitics = function(){
							       for (var m = 0; m < morphemesLen; m++){
								   if (! isClitic($(morphemes[m]))){
								       return false;
								   }   
							       }
							       return true;							   
							   }();							   
							   for (var m = 0; m < morphemesLen; m++){
							       var clitic = isClitic($(morphemes[m]));
							       if ((m === morphemesLen - 2) &&     // i.e. is the penultimate morpheme
								   (! skipPenultClitic) &&
								   (clitic)) 
							       {
								   assignUID($(morphemes[m]), fileName);
							       } else if ((onlyClitics) && 
									  !(m === morphemesLen - 1))  // where all morphemes clitics, last clitic cannot be a head
								   {
								       assignUID($(morphemes[m]), fileName);
								   } else if (clitic) {
								       assignUID($(morphemes[m]), fileName);  
								   }
							   }
						       }
						   });	 
		      };


		      var assignUID = function(item, fileName){
			  if (item.attr("id")){
			      return;
			  }
			  if (UIDs[fileName] && UIDs[fileName].length > 0) {
			      var last = UIDs[fileName].length - 1;
 			      var counter = UIDs[fileName][last] + 1; 
			  } else {
			      UIDs[fileName] = [];
			      var counter = 1; 
			  }
			  UIDs[fileName].push(counter);
			  var UID = fileName + ":" + counter;
			  item.attr("id", UID);	// set the id attribute on the tag		  
		      };

		      var getParagraphsInText = function(xmldataIn){
			  var inTxt = [];
			  $(xmldataIn).find("paragraph").each(function(){
								  inTxt.push(getSentencesInParagraph($(this)));
							      });
			  return {
			      paragraphs: inTxt
			  };
		      };

		      var getSentencesInParagraph = function(paragraph){
			  var inPar = [];
			  $(paragraph).find("s").each(function(){
							  inPar.push(getHeadsInSentence($(this)));
						      });
			  return {
			      sentences: inPar
			  };
		      };


		      var getHeadsInSentence = function(sentence){
			  var headsInSen = [];
			  var senAsString = [];
			  var sen = $(sentence);
			  sen.find("w").each(function(){
						 headsInSen.push(getHeadsInWord($(this)));
						 senAsString.push(getWord($(this)));
					     });
			  var flattenedHeadsInSen = []; // flatten the array of arrays of heads into one single array (preserving order)
			  flattenedHeadsInSen = flattenedHeadsInSen.concat.apply(flattenedHeadsInSen, headsInSen); 
			  var translation = sen.find("trans").text();
			  return {
			      asString: senAsString.join(" "),
			      heads: flattenedHeadsInSen,
			      translation: translation
			  };
		      };

		      var getWord = function(word){
			  var w = [];
			  $(word).find("m").each(function(){						     
						     w.push($(this).text());
						 });			  
			  return w.join("");
		      };

		      var getHeadsInWord = function(word){
			  var jqWord = $(word);
			  var wordUID = jqWord.attr("id");
			  var wordSynrel = jqWord.attr("synrel") ? jqWord.attr("synrel") : "";
			  var wordParent = jqWord.attr("head") ? jqWord.attr("head") : "";
			  var wordGloss = jqWord.attr("gl") ? jqWord.attr("gl") : "";
			  var headsPipe = [];
			  var morphemesToSerialize = [];
			  // 1. clitic(s), host, clitic(s). Y
			  // 2. clitic(s), host. Y
			  // 3. host, clitic(s). Y
			  // 4. word. Y
			  jqWord.find('m').each(function(){
						    if($(this).attr("id")){ // if you've hit a clitic head
							if (morphemesToSerialize.length > 0) {  // serialize any previous non-head morphemes
							    headsPipe.push({
									       gloss: wordGloss,
									       head: wordParent,
									       id: wordUID,
									       pronounced: morphemesToSerialize.join(""),
									       synrel: wordSynrel
									   });
							}
							headsPipe.push({ // i.e. the clitic head
									   head: jqWord.attr("head") ? jqWord.attr("head") : "",
									   id: $(this).attr("id"),
									   pronounced: $(this).text() + '=',
			 						   synrel: jqWord.attr("synrel") ? jqWord.attr("synrel") : ""
								       });
						    }
						    else { // non-head clitic or non-clitic morpheme
							morphemesToSerialize.push($(this).text());
						    }
						});
			  if (morphemesToSerialize.length > 0) {
			      headsPipe.push({   // serialize whatever's left
						 gloss: wordGloss,
						 head: wordParent,
						 id: wordUID,
						 pronounced: morphemesToSerialize.join(""),
						 synrel: wordSynrel
					     });
			  }
			  return headsPipe;
		      };

		      var getValuesFromFieldsets = function (form) {
			  var elementsArr = [];
			  var fieldsets = $(form).find('fieldset').each(function(){
									    var elem = {};
									    var fieldsetID = $(this).attr("id");
									    var legend = $(this).find("legend");
									    elem["headWidth"] = legend.width();
									    elem["headHeight"] = legend.height();
									    elem["fontSize"] = legend.css("font-size");
									    elem["head"] = legend.text();
									    elem["UID"] = fieldsetID;
									    $(this).find('select').each(function(){
													    var name = $(this).attr("name");
													    if (name === "parent"){
														// grab the UID stored in the value attribute, 
														// not the string representation
														var value = $(this).find(":selected").attr("value"); 
													    } else {
														var value = $(this).find(":selected").text();
													    }
													    elem[name] = value;				    
													});
									    var toPush = {};
									    elementsArr.push(elem);
									});
			  return elementsArr;
		      };


		      var calculateSvg = function(formData){
			  var svgData = {};
			  svgData["heads"] = addRelated(formData);
			  svgData["globals"] = getSvgGlobals(formData);
			  return svgData;
		      };

		      var addRelated = function(formData){
			  for (var i = 0, j = formData.length; i < j; i++){
			      if (formData[i]["rel"] === "PRED"){
				  var parent = "nowhere";
				  formData[i]["parent"] = undefined;
			      } else {
				  var parent = formData[i]["parent"];				  
			      }
			      var leftRel = {};
			      var rightRel = {};
			      var head = formData[i];
			      for (var k = 0, l = formData.length; k < l; k++){
				  var otherHead = formData[k];
				  var relOffset = Math.abs(i - k);
				  if ((otherHead["parent"] === head["UID"]) || (otherHead["UID"] === head["parent"])){
				      if (i < k){
					  rightRel[otherHead["UID"]] = relOffset;
				      } else if (i > k) {
					  leftRel[otherHead["UID"]] = relOffset;
				      }				  
				      if (otherHead["UID"] === head["parent"]){
					  head["parentOffset"] = Math.abs(i - k);
					  if (i > k) {
					      var parentTo = "left";					      
					  } else {
					      var parentTo = "right";
					  }
				      }
				  }
			      }
			      var left = extractSortedArr(leftRel);
			      var right = extractSortedArr(rightRel);
			      //console.log(left);
			      //console.log(right);
			      head["related"] = sortRelated(left, right, parent, parentTo);
			  }
			  //console.log(formData);
			  return formData;  
		      };

		      var extractSortedArr = function(obj){
			  var arr = [];
			  for (var prop in obj) {
			      if (obj.hasOwnProperty(prop)) {
				  arr.push({
					       'key': prop,
					       'value': obj[prop]
					   });
			      }
			  }
			  arr.sort(function(a, b) { return a.value - b.value; });
			  var arrOut = [];
			  for (var i = 0, j = arr.length; i < j; i++){
			      arrOut.push(arr[i]["key"]);
			  }
			  //console.log(arrOut);
			  return arrOut; // returns array
		      };

		      var sortRelated = function(left, right, parent, parentTo){

			  var related = {}; 

			  // no parent assigned & head not PRED
			  if (parent === undefined){			      
			      if (left.concat(right).length === 1 || left.length === 0 || right.length === 0) {
				  assignPosition(left.concat(right), "xMid", related);	  
			      } else {				  
				  assignPosition(left, "xLeft", related);
				  assignPosition(right, "xRight", related);
			      }
			      return related;
			  }
			  
			  // head is PRED
			  if (parent === "nowhere") {

			      assignPosition(left, "xLeft", related);
			      assignPosition(right, "xRight", related);			      
			      return related;
			  }

			  // parent has been assigned

			  // left / right flow control switching
			  switch(parentTo){
			  case "right":
			      var arrWithParent = right;
			      var arrWithoutParent = left;
			      break;
			  case "left":				  
			      var arrWithParent = left;
			      var arrWithoutParent = right;
			      break;
			  }
			  var arrWithParentLen = arrWithParent.length;
			  //console.log(arrLen);
			  //console.log(arrWithoutParent.length);
			  // parent assigned & it's the only related element
			  if ((arrWithParentLen === 1) && (arrWithoutParent.length === 0)) {
			      related[parent] = "xMid";
			  } 

			  // parent assigned & there are (potentially) other elements related on the same side of the head
			  else if (arrWithParentLen > 0) {
			      
			      var parentPosition = jQuery.inArray(parent, arrWithParent); // inArray() returns a zero-indexed position
			      console.log(parentPosition);
			      console.log(arrWithParentLen);
			      console.log(arrWithParent);
			      console.log(parent);

			      // parent's first in the array
			      if (parentPosition === 0){ 
				  var restOfArray = arrWithParent.slice(1);
				  switch(parentTo){
				  case "left":
				      related[parent] = "xLeft";
				      assignPosition(restOfArray, "xMid", related);
				      break;
				  case "right":
				      related[parent] = "xMid";
				      assignPosition(restOfArray, "xRight", related);
				      break;
				  }				  
			      } 

			      // parent's last in the array
			      else if (parentPosition + 1 === arrWithParentLen){
				  console.log("parent's last");
				  var restOfArray = arrWithParent.slice(0, -1);
				  switch(parentTo){
				  case "left":
				      related[parent] = "xMid";
				      assignPosition(restOfArray, "xLeft", related);
                                      break;
				  case "right":
				      related[parent] = "xRight";
				      assignPosition(restOfArray, "xMid", related);
				      break;
				  }
			      } 

			      // parent's in the middle of the array
			      else {
				  console.log("parent's in middle");
				  var arrayPartToLeftOfParent = arrWithParent.slice(0, parentPosition);
				  var arrayPartToRightOfParent = arrWithParent.slice(parentPosition + 1);    
				  related[parent] = "xMid";
				  assignPosition(arrayPartToLeftOfParent, "xLeft", related);
				  assignPosition(arrayPartToRightOfParent, "xRight", related);      
			      }

			      // deal with any related heads on non-parent side of head
			      switch(parentTo){
			      case "left":
				  assignPosition(arrWithoutParent, "xRight", related);
				  break;
			      case "right":
				  assignPosition(arrWithoutParent, "xLeft", related);
				  break;				      
			      }
			  }
			  console.log(related);
			  return related;
		      };

		      var assignPosition = function(array, position, related) {
			  for (var a = 0, b = array.length; a < b; a++) {
			      related[array[a]] = position;
			  }			      
		      };

		      var getSvgGlobals = function(heads){
			  var globals = {};
			  var rawWidth = 0;
			  var arrowLevels = 0;
			  var textHeight = 0;
			  for (var i = 0, j = heads.length; i < j; i++){
			      rawWidth += heads[i]["headWidth"];
			      if (heads[i]["parentOffset"] > arrowLevels){
				  arrowLevels = heads[i]["parentOffset"];
			      }
			      if (heads[i]["headHeight"] > textHeight){
				  textHeight = heads[i]["headHeight"];
			      }
			  }

			  globals["rawWidth"] = rawWidth;
			  globals["arrowLevels"] = arrowLevels;
			  globals["numberOfSpaces"] = heads.length - 1;
			  globals["textHeight"] = textHeight;
			  return globals;
		      };


		      var updateXmlSentence = function(formData){
			  
		      };


		      // ---------------------------------------
		      //     present data as html, add into page
		      // ---------------------------------------
		      
		      var textIndexListView = function(textIndex){
			  var indexList = '<ul id="text_index_list">';
			  for (var j=0, k=textIndex.length; j < k; j++){
			      indexList += '<li id="' + textIndex[j] + '"><span>' + textIndex[j] + '</span></li>';
			  };
			  indexList += "</ul>";
			  $("#text_index").append(indexList);
		      };

		      var textView = function(txt, heads){
			  var textViewOut = metadataView(txt) + dataView(heads);
			  if ($('#text').children()){
			      $('#text').children().remove();
			  }
			  $("#text").append(textViewOut);		  
		      };

		      var metadataView = function(txt){
			  var md = $(txt).find("metadata");
			  var textID = $(txt).attr("id");
			  return '<div class="metadata">' 
			      + '<h2>' + textID + ": " + md.find("title").text() + "</h2>"  
			      + "by: " + md.find("author").text()
			      + "<br />date: " + md.find("date").text()
			      + "<br />in: " + md.find("publication").text()
			      + '</div>';			  
		      };

		      var dataView = function(heads){
			  var dataHtml = '<div class="data">';
			  for (var i = 0, j = heads.paragraphs.length; i < j; i++){
			      dataHtml += paragraphView(heads.paragraphs[i], i + 1);
			  }
			  return dataHtml + '</div>';
		      };

		      var paragraphView = function(par, parNum){
			  var parHtml = '<div class="paragraph" id="par' + parNum + '">';
			  for (var k = 0, l = par.sentences.length; k < l; k++){
			      parHtml +=  sentenceView(par.sentences[k], k + 1, parNum);
			  }
			  return parHtml += '</div>';
		      };

		      var sentenceView = function(sentence, senNum, parNum){
			  var senID = 'par' + parNum + 'sen' + senNum; 
			  var senHtml = '<div class="sentence" id="' + senID + '"><form class="sentence_heads hidden" onsubmit="return false;">';
			  senHtml += '<div class="slide-wrap"><div class="inner-wrap">';
			  //console.log(sentence);
			  for (var m = 0, n = sentence.heads.length; m < n; m++){
			      senHtml += headView(sentence.heads[m], sentence.heads);
			  }
			  senHtml += '</div></div><input type="submit" value="Turn this analysis into a graph" class="button" data-reveal-id="svg-'
			      + senID
			      + '"></form>'
			      + '<div class="form_toggle toggled_in">'
			      + '<div class="sentence-as-string">' + sentence.asString 
			      + '</div><div class="translation"><em>' + sentence.translation + '</em></div></div>';
			  return senHtml;
		      };

		      // todo: needs yet to cope with:
		      // todo: - existing synrel, pos and parent
		      // todo: - lemmas for each head to plug into links to dictionary


		      var headView = function(head, heads){
			  //console.log(head);
			  var htmlOut = '<fieldset id="' + head.id + '">' 
			      + '<legend>' + head.pronounced + '</legend>';
			  htmlOut += '<div class="rel_div"><label>rel: </label><select name="rel" class="rel"><option></option>';
			  for (var p = 0, q = synRels.length; p < q; p++) {
			      htmlOut += '<option>' + synRels[p] + '</option>';
			  }
			  htmlOut += '</select></div>';
			  htmlOut += '<div class="parent_div visible"><label>parent: </label><select name="parent" class="parent"><option></option>';
			  for (var p = 0, q = heads.length; p < q; p++){
			      if (heads[p].id === head.id) { 
				  htmlOut += '<option disabled value="' + heads[p].id + '">' + heads[p].pronounced + '</option>';
			      } else {
				  htmlOut += '<option value="' + heads[p].id + '">' + heads[p].pronounced + '</option>';			      
			      }
			  }
			  htmlOut += '</select></div>';
			  htmlOut += '<div class="pos_div"><label>pos:</label><select name="pos" class="pos"><option></option>';
			  for (var p = 0, q = pos.length; p < q; p++){
			      htmlOut += '<option>' + pos[p] + '</option>';
			  };
			  htmlOut += '</select></div></fieldset>';
			  return htmlOut;
		      };

		      var emphasizeParent = function(target){
			  var fieldset = target.closest("fieldset");
			  var fieldsetID = fieldset.attr("id");
			  $(fieldset).siblings().css("background-color", "inherit");			  
			  if(target.is('option')){
			      var parentOption = target.val();
			      if ((parentOption !== fieldsetID) && (parentOption.length !== 0)){
				  $(fieldset).siblings().each(function(){
								  if ($(this).attr("id") === parentOption){
								      $(this).css("background-color", "#ffe");
								  } 
							      });  
			      }
			  }
		      };


		      var deemphasizeParent = function(target){
			  var fieldset = target.closest("fieldset");
			  $(fieldset).siblings().css("background-color", "inherit");			  
		      };

		      var svgView = function(svgInputs, sentenceCssID) {
			  var svgVars = setSvgVars(svgInputs);
			  //console.log(svgInputs);
			  //console.log(svgVars);
			  var svgOut = setSvgTopmatter(svgVars, sentenceCssID)
			             + setSvgDefsAndHeads(svgInputs, svgVars)
			             + setSvgTail();
			  $("#" + sentenceCssID).append(svgOut);
			  var svgID = "#svg-" + sentenceCssID;
			  $(svgID).on($.modal.OPEN, function(event, modal) {
							  $(this).css("width", svgVars["totalWidth"]);  
							});
			  $(svgID).modal(); // to invoke it as a modal
			  $(svgID).on($.modal.CLOSE, function(event, modal) {
							  $(this).remove();  
							});
			  //return svgOut;			  
		      };

		      var setSvgVars = function(svgInputs){
			  return  {
			      spaceInPx:  50,
			      svgPadding: 50,
			      get totalWidth(){
				  return svgInputs["globals"]["rawWidth"] + (this.spaceInPx * svgInputs["globals"]["numberOfSpaces"]) + (2 * this.svgPadding);
			      },
			      arrowStratumHeight: 30,
			      get arrowStrataHeight(){
				  var levels = svgInputs["globals"]["arrowLevels"]; 
				  if (levels <= 1){
				      levels = 2;  // enforce a minimum height;
				  }
				  var cumulativeHeight = 0;
				  for (var i = 1; i <= levels; i++){
				      cumulativeHeight += this.arrowStratumHeight * i;
				  }
				  return cumulativeHeight + (this.svgPadding / 2);				  
			      },
			      textHeight: 25,
			      posHeight: 25,
			      get totalHeight(){
				  return this.arrowStrataHeight + this.textHeight + this.posHeight + (this.svgPadding / 2);
			      },
			      labelPadding: 10,
			      get posY(){
				  return this.arrowStrataHeight + this.textHeight + this.labelPadding + 15;
			      },
			      get textY(){
				  return this.arrowStrataHeight + this.labelPadding + 15;
			      },
			      get arrowsBaseY(){
				  return this.arrowStrataHeight;
				  //return this.posHeight + this.textHeight + (this.svgPadding / 2);
			      },
			      get predArrowTop(){
				  return this.arrowsBaseY - this.arrowStratumHeight - 10;
			      },
			      get predArrowBottom(){
				  return this.arrowsBaseY - 10;
			      },
			      get predLabelY(){
				  return this.predArrowTop - 10;
			      }
			  };
		      };

		      var setSvgTopmatter = function(svgVars, sentenceCssID){			  
			  return '<div id ="svg-' + sentenceCssID + '">'
			      + '<?xml version="1.0" encoding="utf-8" ?>'
			      + '<svg baseProfile="full" version="1.1" xmlns="http://www.w3.org/2000/svg" '
                              + 'xmlns:ev="http://www.w3.org/2001/xml-events" xmlns:xlink="http://www.w3.org/1999/xlink" '
			      + 'width="' + svgVars["totalWidth"] + '" ' 
			      + 'height="' + svgVars["totalHeight"] + '"'
			      + '>';
		      };

		      var setSvgTail = function(){
			  return '</svg></div>';
		      };

		      var setSvgArrowhead = function(){
			  return '<marker id="arrowhead" viewBox="-1 -1 12 12" refX="10" refY="5" markerUnits="strokeWidth" orient="auto" markerWidth="10" markerHeight="10">'
			      + '<polyline points="0,0 10,5 0,10 0,0" fill="white" stroke="black"/>'
			      + '</marker>';  
		      };

		      var setXPos = function(heads, svgVars){
			  var currentX = svgVars["svgPadding"] / 2;
			  for (var i = 0, j = heads.length; i < j; i++){
			      //console.log(currentX);
			      var center = currentX + (heads[i]["headWidth"] / 2);
			      heads[i]["xLeft"] = currentX;
			      heads[i]["xMid"] = center;
			      heads[i]["xRight"] = currentX + heads[i]["headWidth"];
			      currentX = currentX + heads[i]["headWidth"] + svgVars["spaceInPx"];
			  }			  
		      };

		      var setSvgDefsAndHeads = function(svgInputs, svgVars){
			  var heads = svgInputs["heads"];
			  setXPos(heads, svgVars);
			  //console.log(heads);
			  var headsOut = "";
			  var defsOut = "<defs>" + setSvgArrowhead();
			  for (var i = 0, j = heads.length; i < j; i++){
			      var head = heads[i];
			      if (head["rel"] === "PRED"){
				  var predXPos = head["xMid"];
				  var path = "M " + predXPos + " " + svgVars["predArrowTop"] + " L " + predXPos + " " + svgVars["predArrowBottom"];
				  var gTail = '<text x="' + (predXPos - 15) + '" y="' + svgVars["predLabelY"] + '" font-family="monospace">PRED</text>';    
			      } else {
				  var parentUID = head["parent"];
				  for (var k = 0, l = heads.length; k < l; k++){
				      if(heads[k]["UID"] === parentUID){
					  var whichXPosInParent = heads[k]["related"][head["UID"]];
					  var parentEndArrowXPos = heads[k][whichXPosInParent];
				      }
				  }			      
				  var childEndArrowXPos = head[head["related"][parentUID]];
				  var leftmostEnd = childEndArrowXPos;
				  var textanchor = "start";
				  var startOffset = "10%";
				  if (parentEndArrowXPos - childEndArrowXPos < 0){
				      leftmostEnd = parentEndArrowXPos;
				      textanchor = "end";
				      startOffset = '90%';				  
				  }
				  var controlPointOfArrowXPos = leftmostEnd + (Math.abs(parentEndArrowXPos - childEndArrowXPos) / 2) ;
				  var controlPointOfArrowYPos = svgVars["arrowStrataHeight"] 
				      - (1.25 * (svgVars["arrowStratumHeight"] * head["parentOffset"]));
				  var path = 'M ' + parentEndArrowXPos + " " + svgVars["arrowsBaseY"] + " "
				      + 'Q ' + controlPointOfArrowXPos + " " + controlPointOfArrowYPos + " "					  
				      + childEndArrowXPos + " " + svgVars["arrowsBaseY"];
				  var gTail = '<text font-family="monospace" text-anchor="' + textanchor + '">'
				  + '<textPath xlink:href="#rel-' + head["UID"] + '" startOffset="' + startOffset + '">'
				  + head["rel"]
				  + '</textPath></text>'; 
				  var textPathOffset = 5;
				  if (leftmostEnd === childEndArrowXPos) {
				      defsOut += '<path id="rel-' + head["UID"] + '" d="'
					  + 'M ' + childEndArrowXPos + " " + (svgVars["arrowStrataHeight"] - textPathOffset) + " "
					  + 'Q ' + controlPointOfArrowXPos + " " + (controlPointOfArrowYPos - textPathOffset) + " "					  
					  + parentEndArrowXPos + " " + (svgVars["arrowStrataHeight"] - textPathOffset)
					  + '"/>'; 
				      
				  } else {
				      defsOut += '<path id="rel-' + head["UID"] + '" d="'
					  + 'M ' + parentEndArrowXPos + " " + (svgVars["arrowStrataHeight"] - textPathOffset) + " "
					  + 'Q ' + controlPointOfArrowXPos + " " + (controlPointOfArrowYPos - textPathOffset) + " "					  
					  + childEndArrowXPos + " " + (svgVars["arrowStrataHeight"] - textPathOffset)
					  + '"/>'; 
				  }				  
			      }

			      headsOut += '<g id="' + head["UID"] + '">'
				  + '<text x="' + head["xLeft"] + '" y="' + svgVars["textY"] 
				  + '" font-family="monospace" font-size="'+ head["fontSize"] + '">' + head["head"] + '</text>'
				  + '<text x="'+ head["xLeft"] + '" y="' + svgVars["posY"]
				  + '" font-family="monospace">' + head["pos"] + '</text>'
				  + '<path d="' + path
				  + '" stroke="black" fill="transparent" marker-end="url(#arrowhead)"/>'
				  + gTail + '</g>';   
			  }
			  defsOut += "</defs>";
			  return defsOut + headsOut;
		      };

		      var xmlEditorView = function(xmlToWrite){
			  
		      };

		      // -----------
		      //     helpers
		      // -----------

		      var flattenToText = function(node, arr){ 
			  for (var key in node){ 
			      var value = node[key];
			      if (key === "Text"){
				  arr.push(value);
			      } else if (typeof value === 'object'){
				  flattenToText(value);
			      }	
			  }
			  return arr.join(" ");
		      };

		      var sortNumber = function(a,b){
			  return a - b;
		      };

		      var toggle = function(that, toggledInClass, toggledOutClass) {
			  if ($(that).hasClass(toggledInClass)){
			      $(that).removeClass(toggledInClass).addClass(toggledOutClass);
			  } else {
			      $(that).removeClass(toggledOutClass).addClass(toggledInClass);
			  }
		      };

		      var makeVisible = function(that){
			  if ($(that).hasClass("invisible")) {
			      $(that).removeClass("invisible").addClass("visible");
			  }
		      };
		      

		      var makeInvisible = function(that){
			  if ($(that).hasClass("visible")) {
			      $(that).removeClass("visible").addClass("invisible");
			  }
		      };

		      var prettyPrintXml = function(xmlString, indentStep){
			  var splitXml = (xmlString.match(/<[^>]*>/gi));
			  var xmlOut = "";
			  var indentLevel = 0;
			  function indentString(string, indentLevel){
			      var indentation = Array((indentStep * indentLevel) + 1).join(" ");
			      return indentation + string + "\n";        	
			  }
			  for (var i = 0; i < splitXml.length; i++){
    			      var secondChar = splitXml[i].slice(1, 2);
			      var thirdChar = splitXml[i].slice(2, 3);
			      var penultChar = splitXml[i].slice(-2, -1);
			      if (penultChar === "/"){ // A self-closing tag
				  xmlOut += indentString(splitXml[i], indentLevel);
			      } else if (secondChar.match(/[a-zA-Z]/)){ /* An opening tag */ 
				  xmlOut += indentString(splitXml[i], indentLevel);
				  indentLevel++;
			      } else if (secondChar === "/") { /* A closing tag */
				  indentLevel--;
				  xmlOut += indentString(splitXml[i], indentLevel);
			      } else { /* xmlTopMatter */
				  xmlOut += indentString(splitXml[i], indentLevel);
			      };
			  };
			  return xmlOut;
		      };


		      // -----------------------------------
		      //      administer on user interaction
		      // -----------------------------------

		      var getXmlFile = function(filename, successFnc){ 
			  $.ajax({
				     type: "GET",
				     url: "./texts/" + filename,
				     dataType: "xml",
				     success: successFnc,
				     error: function(xhr){
					 var err = eval(xhr.responseText);
					 console.log(err);
				     }
				 });
		      };

		      var populateTextIndexList = function(xml){
			  var til = textIndexList(xml);
			  textIndexListView(til);
		      };

		      var populateText = function(xml){
			  var text = new karukText(xml);
			  // todo: check for being in object already, if Y, ask for whether local or remote version desired 
			  // todo: q. how to compare them? compare side by side as a visual diff?
			  karukTexts[text.filename] = text;  
			  textView(text.text, text.headsInText);
			  //console.log(karukTexts);
		      };

		      var processForm = function(form, sentenceCssID){
			  var analysisFromForm = getValuesFromFieldsets(form);
			  // console.log(analysisFromForm);
			  var dataForSvg = calculateSvg(analysisFromForm);
			  //console.log(dataForSvg);
			  svgView(dataForSvg, sentenceCssID);
			  var xmlToWriteToEditor = updateXmlSentence(analysisFromForm); 
			  xmlEditorView(xmlToWriteToEditor); 
		      };



		      // --------------------------------------------------------
		      //      listen for user interaction, trigger administration
		      // --------------------------------------------------------


		      // switch between closed and open states of "Available Texts"
		      $("#text_index").on("click", function(event){
					      toggle($(this), "toggled_in", "toggled_out");
					  });   

		      // fetch and display which texts are available
		      $("#main-menu_ul").on("click", ".toggled_out", function(event){
						getXmlFile("karuk-text-summary.xml", populateTextIndexList);
					    });

		      // once an available text is selected, fetch it and display it
		      $('#text_index').on("click", "#text_index_list > li", function(event){
					      var clicked = $(this).attr("id");			
					      getXmlFile(clicked, populateText);
					  });
		      
		      // toggle hidden / shown states of form to describe dependency grammar for sentences 
		      $('#text').on("click", ".form_toggle", function(event){
				       toggle($(this), "toggled_in", "toggled_out");
				       toggle($(this).prev(), "shown", "hidden");
				   });

		      // highlight / de-highlight potential parents of a given head when hovering over selection drop-down
		      $("#text").on({mouseover: function(event){
					 emphasizeParent($(event.target));
				     },
				     mouseleave: function(event){
					 deemphasizeParent($(event.target));    
				     }
				    }, ".parent");

		      $("#text").on("change", ".rel", function(event){
					var siblingParentDiv = $(this).parent().siblings(".parent_div");
					if ($(this).find('option:selected').text() === "PRED"){					    
					    makeInvisible($(siblingParentDiv));
					    // todo: reset parent to undefined
					} else {
					    makeVisible($(siblingParentDiv));
					}	
				    });

		      $("#text").on("submit", "form", function(event){
					var sentenceCssID = $(this).parent().attr("id");
					processForm($(this), sentenceCssID);
				    });

	
		  });
