<?xml version="1.0" encoding="UTF-8"?>
   <text id="WB_KL-58">
		<metadata>
			<title>"A Trip to the Land of the Dead"</title>
			<author>Mamie <alph>Offield</alph></author>
			<date>1957</date>
			<publication>William Bright, <em>The Karok Language</em> (1957), pp. 266-269, Text 58</publication>
		</metadata>
		<data>
			<paragraph>
				<s>
					<kar>
						<w>
							<m id="656">asiktávaan</m>
						</w>
						<w gl="her sweetheart">
							<m id="4260">mu</m>
							<m id="3783">keechíkyav</m>
						</w>
						<w>
							<m id="6756">xákaan</m>
						</w>
						<w>
							<m id="6675">vúra</m>
						</w>
						<w>
							<m id="5156">puxích</m>
						</w>
						<w>
							<m id="5101">puráan</m>
						</w>
						<w>
							<m id="5506">tá</m>
						</w>
						<w gl="they liked them">
							<m id="3977">kun</m>
							<m id="5672">tápkuupu</m>
							<m id="6035">tih</m>
						</w>.
					</kar>
					<eng>A woman and her sweetheart loved each other very much.</eng>
				</s>
				<s>	
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w gl="the woman">
							<m id="4407">pa</m>
							<m id="656">'asiktávaan</m>
						</w>
						<w gl="her brothers">
							<m id="4407">pa</m>
							<m id="4260">mu</m>
							<m id="6081">tipáh</m>
							<m id="1724">iivshas</m>
						</w>
						<w gl="they disliked him">
							<m id="3977">kun</m>
							<m id="6522">vîihirimku</m>
							<m id="6035">tih</m>
						</w>.
					</kar>
					<eng>But the woman's brothers disliked (the man).</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="1096">chavúra</m>
						</w>
						<w gl="they killed him">
							<m id="3977">kun</m>
							<m id="3580">íykar</m>
						</w>
						<w gl="the man">
							<m id="4407">pa</m>
							<m id="833">'ávansa</m>
						</w>.
					</kar>
					<eng>Finally they killed the man.</eng>
				</s>
			</paragraph>
			<paragraph>
				<s>
					<kar>
						<w>
							<m id="7157">yukún</m>
						</w>
						<w>
							<m id="6675">vúra</m>
						</w>
						<w>
							<m id="6307">uum</m>
						</w>
						<w>
							<m id="6839">xára</m>
						</w>
						<w>
							<m id="5506">tá</m>
						</w>
						<w gl="they hid themselves">
							<m id="3977">kun</m>
							<m id="1441">'íchunva</m>
						</w>
						<w>
							<m id="53">áasiv</m>
						</w>.
					</kar>
					<eng>You see, (the couple) had hid for a long time in a cave.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="5163">púyava</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w gl="when">
							<m id="4408">pa</m>
							<m id="5506">tá</m>
						</w>
						<w gl="they buried him">
							<m id="3977">kun</m>
							<m id="3131">'íshunva</m>
						</w>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>	
							<m id="6847">xás</m>
						</w>
						<w gl="the woman">
							<m id="4407">pa</m>
							<m id="656">'asiktávaan</m>
						</w>
						<w>
							<m id="4076">kúuk</m>
						</w>
						<w gl="she went">
							<m id="6175">u</m>
							<m id="6306">'uum</m>
						</w>.
					</kar>
					<eng>So when they buried him (there), then the woman went there.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3754">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w>
							<m id="821">ávahkam</m>
						</w>
						<w gl="she laid down on top of it">
							<m id="6175">ú</m>
							<m id="3339">thxuuptakiish</m>
						</w>
						<w gl="the corpse">
							<m id="4407">pa</m>
							<m id="5161">puyâahara</m>
						</w>.
					</kar>
					<eng>And she lay on top of the corpse.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="1096">chavúra</m>
						</w>
						<w gl="she had">
							<m id="5506">t</m>
							<m id="6175">óo</m>
						</w>
						<w>
							<m id="3957">kúha</m>
						</w>,
						<w gl="it had">
							<m id="5506">t</m>
							<m id="6175">óo</m>
						</w>
						<w>
							<m id="2490">mpux</m>
						</w>
						<w gl="the corpse">
							<m id="4407">pa</m>
							<m id="5161">puyâahara</m>
						</w>.
					</kar>
					<eng>Finally she got sick, the corpse was swelling.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w gl="she thought">
							<m id="6175">u</m>
							<m id="6939">xus</m>
						</w>, "
						<w>
							<m id="5506">tá</m>
						</w>
						<w gl="I am sick">
							<m id="4296">na</m>
							<m id="3957">kúha</m>
						</w>, 
						<w>
							<m id="6738">xâatik</m>
						</w>
						<w>
							<m id="6675">vúra</m>
						</w>
						<w gl="I come out again">
							<m id="4349">ni</m>
							<m id="2674">p</m>
							<m id="6450">váruprav</m>
						</w>."
					</kar>
					<eng>And she said, "I'm sick, let me go out!"</eng>
				</s>
			</paragraph>
			<paragraph>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w>
							<m id="6383">vaa</m>
						</w>
						<w gl="when she slept">
							<m id="4408">p</m>
							<m id="6175">óo</m>
							<m id="2189">kviit-ha</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w gl="she dreamed about him">
							<m id="6175">u</m>
							<m id="2221">kvit-hûunish</m>
						</w>.
					</kar>
					<eng>Then when she slept, she dreamed about him.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w gl="he said">
							<m id="6175">u</m>
							<m id="4823">piip</m>
						</w>, "
						<w>
							<m id="1516">ifuyâach</m>
						</w>
						<w>
							<m id="1402">húm</m>
						</w>
						<w gl="that you feel sorry for me">
							<m id="4408">pa</m>
							<m id="5506">ta</m>
							<m id="4294">na</m>
							<m id="3913">koohímachva</m>
						</w>."
					</kar>
					<eng>And he said, "Is it true that you grieve for me?"</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w gl="he said">
							<m id="6175">u</m>
							<m id="4823">piip</m>
						</w>, "
						<w gl="if true">
							<m id="4408">pa</m>
							<m id="1456">'íf</m>
							<m id="1358">haak</m>
						</w>
						<w>
							<m id="6570">víri</m>
						</w>
						<w>
							<m id="1144">chími</m>
						</w>
						<w gl="let me tell you">
							<m id="4370">nu</m>
							<m id="2733">pêen</m>
						</w>
						<w gl="how you will do">
							<m id="4408">p</m>
							<m id="1418">ée</m>
							<m id="4086">kuph</m>
							<m id="1379">eesh</m>
						</w>.
					</kar>
					<eng>And he said, "If it is true, let me tell you what to do.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="6383">vaa</m>
						</w>
						<w>
							<m id="1729">ík</m>
						</w>
						<w>
							<m id="3670">káan</m>
						</w>
						<w gl="you will go">
							<m id="1418">i</m>
							<m id="6306">'uum</m>
							<m id="1379">êesh</m>
						</w>
						<w gl="where (in the near past)">
							<m id="4408">pá</m>
							<m id="4248">mita</m>
						</w>
						<w gl="where we were staying all the time">
							<m id="4370">nu</m>
							<m id="2664">'ínoohvoo</m>
							<m id="6035">tih</m>
							<m id="1387">irak</m>
						</w>
						<w gl="in the cave">
							<m id="4407">pa</m>
							<m id="53">'aasív</m>
							<m id="232">ak</m>
						</w>.
					</kar>
					<eng>You must go there where we used to stay, in the cave.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="6570">víri</m>
						</w>
						<w>
							<m id="6383">vaa</m>
						</w>
						<w gl="you will see it">
							<m id="1418">i</m>
							<m id="4155">máh</m>
							<m id="1379">eesh</m>
						</w>
						<w>
							<m id="1002">axvithínih</m>
						</w>.
					</kar>
					<eng>You will see a grave there.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3752">káru</m>
						</w>
						<w gl="you will see them">
							<m id="1418">i</m>
							<m id="4155">máh</m>
							<m id="1379">eesh</m>
						</w>
						<w>
							<m id="864">áxak</m>
						</w>
						<w>
							<m id="7239">yuup</m>
						</w>
						<w gl="it will be floating around">
							<m id="6175">ú</m>
							<m id="3357">thyiimvarayv</m>
							<m id="1379">eesh</m>
						</w>.
					</kar>
					<eng>And you will see two eyes float around.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="6881">xáyfaat</m>
						</w>
						<w>
							<m id="1729">ík</m>
						</w>
						<w gl="you fear me">
							<m id="4294">ná</m>
							<m id="1019">'ay</m>
						</w>.
					</kar>
					<eng>You mustn't be afraid of me.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="6881">xáyfaat</m>
						</w>
						<w>
							<m id="1729">ík</m>
						</w>
						<w>
							<m id="2195">íkvip</m>
						</w>."
					</kar>
					<eng>You mustn't run.</eng>
				</s>
			</paragraph>
			<paragraph>
				<s>
					<kar>
						<w>
							<m id="6174">ta'ítam</m>
						</w>
						<w>
							<m id="6381">vaa</m>
						</w>
						<w>
							<m id="4077">kúuk</m>
						</w>
						<w gl="she went">
							<m id="6175">u</m>
							<m id="6306">'uum</m>
							<m id="1378">áheen</m>
						</w>.
					</kar>
					<eng>So she went there.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w>
							<m id="6381">vaa</m>
						</w>
						<w>
							<m id="6675">vúra</m>
						</w>
						<w gl="she saw it">
							<m id="6175">u</m>
							<m id="4155">mah</m>
						</w>.
					</kar>
					<eng>And she saw that.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w>
							<m id="924">axmáy</m>
						</w>
						<w gl="it spoke">
							<m id="6175">u</m>
							<m id="1218">chúupha</m>
						</w>.
					</kar>
					<eng>And suddenly (a voice) spoke.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w gl="it said">
							<m id="6175">u</m>
							<m id="4823">piip</m>
						</w>, "
						<w gl="you will weave it">
							<m id="1418">i</m>
							<m id="6545">vík</m>
							<m id="1379">eesh</m>
						</w>
						<w>
							<m id="1729">ík</m>
						</w>
						<w>
							<m id="789">átimnam</m>
						</w>.
					</kar>
					<eng>And it said, "You must weave a burden basket.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3752">káru</m>
						</w>
						<w>
							<m id="5541">taay</m>
						</w>
						<w>
							<m id="1729">ík</m>
						</w>
						<w>
							<m id="6675">vúra</m>
						</w>
						<w>
							<m id="7028">yáfus</m>
						</w>
						<w gl="you will make it">
							<m id="1418">i</m>
							<m id="2348">kyâa</m>
							<m id="1379">vish</m>
						</w>.
					</kar>
					<eng>And you must make many dresses.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="6570">víri</m>
						</w>
						<w>
							<m id="6383">vaa</m>
						</w>
						<w gl="when you finish it">
							<m id="4408">p</m>
							<m id="1418">ee</m>
							<m id="4903">píkyaar</m>
							<m id="1358">ahaak</m>
						</w>
						<w>
							<m id="6570">víri</m>
						</w>
						<w>
							<m id="6383">vaa</m>
						</w>
						<w gl="you will see it">
							<m id="1418">i</m>
							<m id="4155">máh</m>
							<m id="1379">eesh</m>
						</w> '
						<w>
							<m id="6383">vaa</m>
						</w>
						<w>
							<m id="3670">káan</m>
						</w>
						<w gl="top of a rock">
							<m id="583">asa</m>
							<m id="2697">'ípan</m>
						</w>
						<w gl="it sits">
							<m id="6175">ú</m>
							<m id="1974">krii</m>
						</w>
						<w>
							<m id="792">atipimáamvaan</m>
						</w>.'
					</kar>
					<eng>When you finish, you will see a buzzard sit there on top of a rock.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="6570">víri</m>
						</w>
						<w>
							<m id="6383">vaa</m>
						</w>
						<w>
							<m id="1729">ík</m>
						</w>
						<w gl="you will follow it"> 
							<m id="1418">i</m>
							<m id="180">'áharam</m>
							<m id="1379">eesh</m>
						</w>.
					</kar>
					<eng>You must follow it.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="7035">yakún</m>
						</w>
						<w>
							<m id="6381">vaa</m>
						</w>
						<w gl="dead person bird">
							<m id="7158">yumaar</m>
							<m id="119">áachviiv</m>
						</w>."
					</kar>
					<eng>You see, that is the bird of the dead."</eng>
				</s>
			</paragraph>
			<paragraph>
				<s>
					<kar>
						<w>
							<m id="5163">púyava</m>
						</w>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="3857">xás</m>
						</w>
						<w>
							<m id="6174">ta'ítam</m>
						</w>
						<w gl="she wove"> 
							<m id="6175">u</m>
							<m id="6545">vík</m>
							<m id="1378">aheen</m>
						</w>.
					</kar>
					<eng>And so then she wove.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w>
							<m id="656">asiktâan</m>
						</w>
						<w gl="she told her">
							<m id="6175">u</m>
							<m id="2733">péer</m>
						</w>, "
						<w gl="let's go together!">
							<m id="4370">nu</m>
							<m id="6758">xákaanh</m>
							<m id="1419">i</m>
						</w>."
					</kar>
					<eng>And she said to a woman, "Let's go together!"</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="6675">vúra</m>
						</w>
						<w>
							<m id="6307">uum</m>
						</w>
						<w gl="her friend">
							<m id="4260">mú</m>
							<m id="167">fyiiv</m>
						</w>.
					</kar>
					<eng>She was her friend.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="6174">ta'ítam</m>
						</w>
						<w>
							<m id="6307">uum</m>
						</w>
						<w>
							<m id="3752">káru</m>
						</w>
						<w gl="she wove"> 
							<m id="6175">u</m>
							<m id="6545">vík</m>
							<m id="1378">aheen</m>
						</w>
						<w>
							<m id="3752">káru</m>
						</w>
						<w gl="she made them">
							<m id="6175">ú</m>
							<m id="2348">kyav</m>
						</w>
						<w gl="the dresses">
							<m id="4407">pa</m>
							<m id="7028">yáfus</m>
						</w>.
					</kar>
					<eng>So she too wove and made the dresses.</eng>
				</s>
			</paragraph>
			<paragraph>
				<s>
					<kar>
						<w>
							<m id="5163">púyava</m>
						</w>
						<w>
							<m id="5506">tá</m>
						</w>
						<w gl="they finished">
							<m id="3977">kun</m>
							<m id="4903">píkyaar</m>
						</w>.
					</kar>
					<eng>Then they finished.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w gl="they left">
							<m id="3977">kun</m>
							<m id="3562">iyâaram</m>
						</w>.
					</kar>
					<eng>So they left.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w gl="they saw it">
							<m id="3977">kun</m>
							<m id="4155">mah</m>
						</w>, 
						<w gl="the buzzard">
							<m id="4407">pa</m>
							<m id="792">'atipimáamvaan</m>
						</w>.
					</kar>
					<eng>And they saw the buzzard.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="6174">ta'ítam</m>
						</w>
						<w gl="they followed it">
							<m id="3977">kun</m>
							<m id="180">'áharam</m>
							<m id="1378">aheen</m>
						</w>.
					</kar>
					<eng>So they followed it.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w gl="they traveled">
							<m id="3977">kun</m>
							<m id="205">'áhoo</m>
						</w>, 
						<w>
							<m id="6675">vúra</m>
						</w>
						<w>
							<m id="6307">uum</m>
						</w>
						<w>
							<m id="5541">taay</m>
						</w>
						<w>
							<m id="5468">súpaa</m>
						</w>
						<w gl="that they traveled">
							<m id="4408">pa</m>
							<m id="3977">kun</m>
							<m id="205">'áhoo</m>
						</w>.
					</kar>
					<eng>And they traveled, it was many days that they traveled.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="6675">vúra</m>
						</w>
						<w>
							<m id="6383">vaa</m>
						</w>
						<w gl="they were following it">
							<m id="3977">kun</m>
							<m id="180">'áharamu</m>
							<m id="6035">ti</m>
						</w>
						<w gl="the buzzard">
							<m id="4407">pa</m>
							<m id="792">'atipimáamvaan</m>
						</w>.
					</kar>
					<eng>They were following the buzzard that way.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w>
							<m id="1361">hâari</m>
						</w>
						<w>
							<m id="6675">vúra</m>
						</w>
						<w gl="brushy place">
							<m id="4963">pirísh</m>
							<m id="5180">riik</m>
						</w>
						<w gl="where">
							<m id="4408">pa</m>
							<m id="5506">tá</m>
						</w>
						<w gl="they traveled">
							<m id="3977">kun</m>
							<m id="205">'áhoo</m>
						</w>, 
						<w gl="their dresses"> 
							<m id="4407">pa</m>
							<m id="4264">mukun</m>
							<m id="7028">yáfus</m>
						</w>
						<w gl="it had become tattered"> 
							<m id="5506">t</m>
							<m id="6175">u</m>
							<m id="5741">tatitítit</m>
						</w>.
					</kar>
					<eng>And sometimes it was a brushy place where they traveled, their dresses got torn.</eng>
				</s>
			</paragraph>
			<paragraph>
				<s>
					<kar>
						<w>
							<m id="1096">chavúra</m>
						</w>
						<w gl="they arrived">
							<m id="3977">kun</m>
							<m id="6306">'uum</m>
						</w>, 
						<w>
							<m id="6675">vúra</m>
						</w>
						<w>
							<m id="6307">uum</m>
						</w>
						<w>
							<m id="7000">yâamach</m>
						</w>
						<w gl="the country">
							<m id="4407">p</m>
							<m id="3257">eethívthaaneen</m>
						</w>, 
						<w>
							<m id="3850">kípa</m>
						</w>
						<w>
							<m id="6014">thúkin</m>
						</w>.
					</kar>
					<eng>Finally they arrived, the country was beautiful and green.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w>
							<m id="6174">ta'ítam</m>
						</w>
						<w gl="they rowed to meet them">
							<m id="3843">kin</m>
							<m id="6592">vítivrik</m>
							<m id="1378">aheen</m>
						</w>,
						<w>
							<m id="3347">ithyárukirum</m>
						</w>
						<w gl="they landed them">
							<m id="3843">kin</m>
							<m id="6591">vítish</m>
						</w>.
					</kar>
					<eng>And someone rowed to meet them and landed them on the other shore.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w>
							<m id="7038">yánava</m>
						</w>
						<w>
							<m id="864">áxak</m>
						</w>
						<w>
							<m id="3670">kaan</m>
						</w>
						<w gl="old women">
							<m id="3804">kéevniikich</m>
							<m id="584">as</m>
						</w>.
					</kar>
					<eng>And they saw two old women there.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w gl="they told her">
							<m id="3977">kun</m>
							<m id="2733">ipéer</m>
						</w>, "
						<w>
							<m id="4132">mâa</m>
						</w>
						<w>
							<m id="3669">kâam</m>
						</w>
						<w>
							<m id="6642">vuhvúha</m>
						</w>
						<w gl="he is making it">
							<m id="6175">u</m>
							<m id="2348">kyáa</m>
							<m id="6035">ti</m>
						</w>
						<w gl="the one for whose sake"> 
							<m id="4408">pa</m>
							<m id="4059">kúth</m>
						</w>
						<w gl="you are wandering around">
							<m id="1418">i</m>
							<m id="6683">vúrayvu</m>
							<m id="6035">tih</m>
						</w>.
					</kar>
					<eng>And (the old woman) said, "Look, the one you are wandering around for is making a deerskin dance uphill.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="1281">fâat</m>
						</w>
						<w>
							<m id="3971">kumá'ii</m>
						</w>
						<w gl="that here">
							<m id="4408">pa</m>
							<m id="4385">'ôok</m>
						</w>
						<w gl="you have traveled"> 
							<m id="5506">t</m>
							<m id="1418">i</m>
							<m id="205">'áhoo</m>
						</w>.
					</kar>
					<eng>Why is it that you have come here?</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="4385">ôok</m>
						</w>
						<w>
							<m id="6307">uum</m>
						</w>
						<w gl="he doesn't travel">
							<m id="5082">pu</m>
							<m id="205">'áhoo</m>
							<m id="6035">tih</m>
							<m id="1371">ara</m>
						</w>
						<w gl="the one who has bones">
							<m id="4407">pa</m>
							<m id="2750">'ípi</m>
							<m id="1356">hi</m>
							<m id="6035">tih</m>
							<m id="369">an</m>
						</w>.
					</kar>
					<eng>People with bones (i.e., live people) don't come here.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="6570">víri</m>
						</w>
						<w>
							<m id="1192">chôora</m>
						</w>
						<w>
							<m id="1144">chími</m>
						</w>
						<w gl="let us hide you!">
							<m id="4370">nu</m>
							<m id="3131">'íshunv</m>
							<m id="1419">i</m>
						</w>.
					</kar>
					<eng>Come on, let's hide you!</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="6879">xáy</m>
						</w>
						<w gl="they see you guys">
							<m id="3830">kíik</m>
							<m id="4155">mah</m>
							<m id="398">ap</m>
						</w>."
					</kar>
					<eng>Let them not see you!</eng>
				</s>
			</paragraph>
			<paragraph>
				<s>
					<kar>
						<w>
							<m id="6174">ta'ítam</m>
						</w>
						<w gl="they hid them">
							<m id="3843">kin</m>
							<m id="3131">'íshunv</m>
							<m id="1378">aheen</m>
						</w>.
					</kar>
					<eng>So they hid them.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="5163">púyava</m>
						</w>
						<w>
							<m id="6675">vúra</m>
						</w>
						<w>
							<m id="6781">xánahishich</m>
						</w>
						<w>
							<m id="3670">káan</m>
						</w>
						<w>
							<m id="5506">tá</m>
						</w>
						<w gl="they stayed">
							<m id="3977">kun</m>
							<m id="1664">'iin</m>
						</w>.
					</kar>
					<eng>So they stayed there for a little while.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w gl="they told them">
							<m id="3843">kin</m>
							<m id="2733">ipéer</m>
						</w>, "
						<w>
							<m id="1144">chími</m>
						</w>
						<w gl="you guys go back!">
							<m id="3829">kiik</m>
							<m id="5047">piyâaram</m>
							<m id="1419">i</m>
						</w>."
					</kar>
					<eng>Then they were told, "Go back home!"</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w gl="they gave it to them"> 
							<m id="3843">kin</m>
							<m id="249">'ákih</m>
						</w>
						<w>
							<m id="366">amveeváxrah</m>
						</w>.
					</kar>
					<eng>And they were given dried salmon.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="1383">hínupa</m>
						</w>
						<w>
							<m id="126">achvuun</m>
						</w>.
					</kar>
					<eng>There it was dog salmon.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="7157">yukún</m>
						</w>
						<w>
							<m id="6383">vaa</m>
						</w>
						<w gl="they call it">
							<m id="3977">kun</m>
							<m id="2733">ípeen</m>
							<m id="6035">ti</m>
						</w>
						<w>
							<m id="7159">yumaará'aama</m>
						</w>
						<w>
							<m id="126">achvuun</m>
						</w>.
					</kar>
					<eng>You see, they call dog salmon "dead-man's salmon."</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w gl="they told them">
							<m id="3843">kin</m>
							<m id="2733">ipéer</m>
						</w>, "
						<w gl="when a person">
							<m id="4408">pa</m>
							<m id="506">'áraar</m>
						</w>
						<w gl="he has died">
							<m id="5506">t</m>
							<m id="6175">u</m>
							<m id="3441">'ív</m>
							<m id="1358">ahaak</m>
						</w>, 
						<w>
							<m id="6381">vaa</m>
						</w>
						<w>
							<m id="1729">ík</m>
						</w>
						<w>
							<m id="427">apmántiim</m>
						</w>
						<w gl="you guys will be rubbing it on them">
							<m id="3939">ku</m>
							<m id="3642">yvúruk</m>
							<m id="6035">tih</m>
							<m id="1379">eesh</m>
						</w>.
					</kar>
					<eng>And they were told, "When a person dies, you must rub this on his lips.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="7035">yakún</m>
						</w>
						<w gl="he will come back to life">
							<m id="6175">u</m>
							<m id="4925">pimtáv</m>
							<m id="1379">eesh</m>
						</w>."
					</kar>
					<eng>You see, he will come back to life."</eng>
				</s>
			</paragraph>
			<paragraph>
				<s>
					<kar>
						<w>
							<m id="6174">ta'ítam</m>
						</w>
						<w gl="they went back">
							<m id="3977">kun</m>
							<m id="5047">piyâaram</m>
							<m id="1378">aheen</m>
						</w>.
					</kar>
					<eng>So (the girls) went back home.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3969">kúkuum</m>
						</w>
						<w>
							<m id="6675">vúra</m>
						</w>
						<w>
							<m id="6383">vaa</m>
						</w>
						<w gl="they traveled back">
							<m id="3977">kun</m>
							<m id="2690">'ípahoo</m>
						</w>.
					</kar>
					<eng>They traveled back again that way.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="6383">vaa</m>
						</w>
						<w>
							<m id="6675">vúra</m>
						</w>
						<w gl="the buzzard">
							<m id="4407">pa</m>
							<m id="792">'atipimaamvan</m>
							<m id="1670">'îin</m>
						</w>
						<w gl="it brought them back">
							<m id="3843">kin</m>
							<m id="2674">p</m>
							<m id="4398">ôonvuuk</m>
						</w>.
					</kar>
					<eng>The buzzard brought them back.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="5163">púyava</m>
						</w>
						<w gl="when they returned">
							<m id="4408">pa</m>
							<m id="3977">kun</m>
							<m id="2694">'ípak</m>
						</w>
						<w>
							<m id="4385">ôok</m>
						</w>
						<w gl="its world">
							<m id="3970">kumee</m>
							<m id="3257">thívthaaneen</m>
						</w>
						<w>
							<m id="6381">vaa</m>
						</w>
						<w>
							<m id="6307">uum</m>
						</w>
						<w gl="the ones who did it">
							<m id="4407">pa</m>
							<m id="4086">kúph</m>
							<m id="32">aan</m>
							<m id="1369">hanik</m>
						</w>
						<w gl="as one is doing it">
							<m id="4408">p</m>
							<m id="6175">óo</m>
							<m id="4086">kupi</m>
							<m id="6035">ti</m>
						</w>
						<w>
							<m id="506">áraar</m>
						</w>
						<w gl="where one is dead">
							<m id="6175">u</m>
							<m id="5518">tâanaxihi</m>
							<m id="6035">tih</m>
							<m id="2889">irak</m>
						</w>.
					</kar>
					<eng>So when they returned to this world, they are the ones who did as it is done in the land of the dead.</eng>
				</s>
			</paragraph>
			<paragraph>
				<s>
					<kar>
						<w>
							<m id="6570">víri</m>
						</w>
						<w>
							<m id="1096">chavúra</m>
						</w>
						<w gl="no person">
							<m id="5082">pu</m>
							<m id="506">'áraar</m>
						</w>
						<w gl="he was not dying">
							<m id="3441">iim</m>
							<m id="6035">tih</m>
							<m id="1371">ara</m>
						</w>, 
						<w>
							<m id="1096">chavúra</m>
						</w>
						<w gl="the world">
							<m id="4407">p</m>
							<m id="3257">eethívthaaneen</m>
						</w>
						<w gl="they filled it">
							<m id="6175">u</m>
							<m id="2674">p</m>
							<m id="1009">áxyar</m>
						</w>
						<w gl="the people">
							<m id="4407">pa</m>
							<m id="506">'áraar</m>
						</w>.
					</kar>
					<eng>Finally no person died, finally the people filled up the earth.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="6383">vaa</m>
						</w>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w>
							<m id="6675">vúra</m>
						</w>
						<w gl="they were dying">
							<m id="3977">kun</m>
							<m id="3441">'íim</m>
							<m id="6035">ti</m>
						</w>
						<w gl="when it was all gone">
							<m id="4408">p</m>
							<m id="6175">oo</m>
							<m id="1313">fíipha</m>
						</w>
						<w gl="the salmon">
							<m id="4407">pa</m>
							<m id="30">'áama</m>
						</w>.
					</kar>
					<eng>Then when the salmon was all gone, they died.</eng>
				</s>
			</paragraph>
		</data>
	</text>
	
							
							
							
							
								
							
							
						
						
							
					
							
						
							
						
							
						
							
					
							
							
							
						
							
							
								
							
				
							
					
							
						
						
						
					
					
						
						
							
						
							
						
							
					
							
						
							
							
						
						
							
							
						
				
						
						 
							
							
							
							
						
							
						
						
						
						
				