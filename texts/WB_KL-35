<?xml version="1.0" encoding="UTF-8"?>
<text id="WB_KL-35">
    <metadata>
        <title>"The Story of Madrone"</title>
        <author>Lottie <alph>Beck</alph></author>
        <date>1957</date>
        <publication>William Bright, <em>The Karok Language</em> (1957), pp. 236-237, Text 35</publication>
    </metadata>
    <data>
        <paragraph>
            <s>
                <kar>
                    <w>
						<m id="4039">kusrípan</m>
					</w>
					<w>
						<m id="6307">uum</m>
					</w>
					<w>
						<m id="3219">itháan</m>
					</w>
					<w gl="was a man">
						<m id="833">avansa</m> 
						<m id="1369">hanik</m>
					</w>.
				</kar>
				<eng>Madrone was once a man.</eng>
			</s>
			 <s>
                <kar>
                    <w gl="rich man">
						<m id="506">arara</m>
						<m id="7022">yaas'ára</m>
					</w>
					<w gl="his son">
						<m id="4260">mu</m>
						<m id="143">'afishríhan</m>
					</w>.
				</kar>
				<eng>He was a rich man's son.</eng>
			</s>	
            <s>
                <kar>
                    <w>
						<m id="6847">xás</m>
					</w>
					<w gl="downriver across-stream from them">
						<m id="4264">mukun</m>
						<m id="7244">yûuch</m>
						<m id="3731">kam</m>
						<m id="83">ach</m>
					</w>
					<w gl="he lived">
						<m id="6175">ú</m>
						<m id="1974">krii</m>
					</w>.
				</kar>
				<eng>And a man lived downriver across-stream from them.</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="6675">vúra</m>
					</w>
					<w>
						<m id="7000">yâamach</m>
					</w>
					<w gl="his daughter">
						<m id="4260">mu</m>
						<m id="1459">'ifápiit</m>
					</w>.
				</kar>
				<eng>His daughter was pretty.</eng>	
			</s>
			<s>
				<kar>
					<w>
						<m id="6847">xás</m>
					</w>
					<w>
						<m id="3671">kâanimich</m>
					</w>
					<w gl="they were living in that way">
						<m id="3977">kun</m>
						<m id="4004">kupa</m>
						<m id="508">'áraarahi</m>
						<m id="6035">tih</m>
					</w>.
				</kar>
				<eng>But they lived poorly.</eng>	
			</s>
			<s>
				<kar>
					<w>
						<m id="6570">víri</m>
					</w>
					<w>
						<m id="6308">ûum</m>
					</w>
					<w>
						<m id="5506">tá</m>
					</w>
					<w gl="they lived through winter">
						<m id="3977">kun</m>
						<m id="3180">íshyaavha</m>
					</w>.
				</kar>
				<eng>They barely lived through the winter.</eng>	
			</s>
			<s>
				<kar>
					<w>
						<m id="3670">káan</m>
					</w>
					<w>
						<m id="4357">ník</m>
					</w>
					<w gl="the man">
						<m id="4407">pa</m>
						<m id="833">'ávansa</m>
					</w>
					<w gl="he hunts">
						<m id="6175">u</m>
						<m id="304">'ákunvu</m>
						<m id="6035">ti</m>
					</w>
					<w>
						<m id="3752">káru</m>
					</w>
					<w gl="he fishes">
						<m id="6175">u</m>
						<m id="188">'ahavishkâavu</m>
						<m id="6035">tih</m>
					</w>.
				</kar>
				<eng>The man would hunt and fish there.</eng>	
			</s>
			<s>
				<kar>
					<w>
						<m id="6682">vúrava</m>
					</w>
					<w>
						<m id="3931">kooyâach</m>
					</w>
					<w gl="he came back">
						<m id="5506">t</m>
						<m id="6175">u</m>
						<m id="1692">'íipma</m>
					</w>.
				</kar>
				<eng>He would come back with just as much (as he started out with, i.e. nothing).</eng>	
			</s>
			<s>
				<kar>
					<w>
						<m id="2392">imáankam</m>
					</w>
					<w>
						<m id="3969">kúkuum</m>
					</w>
					<w gl="he left">
						<m id="5506">t</m>
						<m id="6175">u</m>
						<m id="6396">vâaram</m>
					</w>.
				</kar>
				<eng>The next day he would go again.</eng>	
			</s>
			<s>
				<kar>
					<w>
						<m id="6675">vúra</m>
					</w>
					<w gl="they (the spirits) inflicted bad luck on him">
						<m id="3977">kun</m>
						<m id="5050">píychaak</m>
						<m id="6035">tih</m>
					</w>.
				</kar>
				<eng>He had bad luck.</eng>	
			</s>	
			<s>
				<kar>
					<w>
						<m id="6675">vúra</m>
					</w>
					<w>
						<m id="6307">uum</m>
					</w>
					<w gl="never">
						<m id="5082">pu</m>
						<m id="1377">haríxay</m>
					</w>
					<w>
						<m id="7049">yav</m>
					</w>
					<w gl="they were not living in that way">
						<m id="4004">kupa</m>
						<m id="508">'áraarahi</m>
						<m id="6035">tih</m>
						<m id="399">ap</m>
					</w>.
				</kar>
				<eng>They never lived well.</eng>	
			</s>		
		</paragraph>
		<paragraph>
			<s>
				<kar>
					<w>
						<m id="4039">kusrípan</m>
					</w>
					<w>
						<m id="6307">uum</m>
					</w>
					<w>
						<m id="6675">vúra</m>
					</w>
					<w>
						<m id="3382">itíhaan</m>
					</w>
					<w gl="he was wandering idly">
						<m id="6175">u</m>
						<m id="6650">vunayvîichvu</m>
						<m id="6035">tih</m>
					</w>.
				</kar>
				<eng>Madrone was always wandering around idly.</eng>	
			</s>
			<s>
				<kar>
					<w>
						<m id="3753">káruk</m>
					</w>
					<w gl="he was going upriver">
						<m id="6175">u</m>
						<m id="558">'árih</m>
						<m id="5191">roona</m>
						<m id="6035">tih</m>
					</w>.
				</kar>
				<eng>He kept going upriver.</eng>	
				<note publish="yes">Bright: "This seems to indicate that the girl lives upstream, in contradiction to the statement of sentence 3. Succeeding sentences are consistent with sentence 13, rather than sentence 3."</note>
			</s>
			<s>
				<kar>
					<w>
						<m id="6847">xás</m>
					</w>
					<w gl="the young woman">
						<m id="4407">pa</m>
						<m id="1459">'ifápiit</m>
					</w>
					<w gl="there across-river and upriver">
						<m id="3926">kôoth</m>
						<m id="3731">kam</m>
					</w>
					<w gl="he was looking across-river">
						<m id="6175">u</m>
						<m id="3390">tkára</m>
						<m id="6035">tih</m>
					</w>.
				</kar>
				<eng>And he saw the girl upriver across-stream.</eng>	
			</s>
			<s>
				<kar>
					<w gl="she was seeing him coming">
						<m id="6175">u</m>
						<m id="4189">mahunâana</m>
						<m id="6035">tih</m>
					</w>.
				</kar>
				<eng>She saw him coming.</eng>	
			</s>
			<s>
				<kar>
					<w>
						<m id="6847">xás</m>
					</w>
					<w gl="she whistled at him">
						<m id="6175">u</m>
						<m id="1797">kfuyvûunish</m>
					</w>.
				</kar>
				<eng>And she whistled at him.</eng>	
			</s>
			<s>
				<kar>
					<w>
						<m id="6847">xás</m>
					</w>
					<w gl="he stood still">
						<m id="6175">u</m>
						<m id="1617">hyárihish</m>
					</w>.
				</kar>
				<eng>And he stopped.</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="2392">imáankam</m>
					</w>
					<w>
						<m id="3969">kúkuum</m>
					</w>
					<w>
						<m id="6675">vúra</m>
					</w>
					<w gl="he went there from downhill">
						<m id="6175">u</m>
						<m id="558">'árih</m>
						<m id="5171">raa</m>
					</w>.
				</kar>
				<eng>The next day he went up again.</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="6847">xás</m>
					</w>
					<w gl="when she saw him coming">
						<m id="4408">p</m>
						<m id="6175">oo</m>
						<m id="4161">máhavrik</m>
					</w>
					<w gl="she swam across">
						<m id="6175">ú</m>
						<m id="1922">kpuuhrin</m>
					</w>.
				</kar>
				<eng>And when she saw him coming, she swam across.</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="1384">hínu páy</m>
					</w>
					<w>
						<m id="1109">chí</m>
					</w>
					<w gl="they will chat">
						<m id="3977">kun</m>
						<m id="1207">chúphiichv</m>
						<m id="1379">eesh</m>
					</w>.
				</kar>
				<eng>There they were going to chat.</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="6847">xás</m>
					</w>
					<w gl="for too long">
						<m id="6839">xára</m>
						<m id="1389">hirurav</m>
					</w>
					<w gl="he was absent">
						<m id="6175">u</m>
						<m id="5414">sínmoo</m>
					</w>
					<w>
						<m id="4039">kusrípan</m>
					</w>.
				</kar>
				<eng>But Madrone stayed away too long.</eng>
			</s>
			<s>
				<kar>
					<w gl="his father">
						<m id="4407">pa</m>
						<m id="4260">mu</m>
						<m id="236">'akah</m>
						<m id="1670">'îin</m>
					</w>
					<w gl="he went to search for him">
						<m id="3977">kun</m>
						<m id="4545">pápivar</m>
					</w>.
				</kar>
				<eng>His father went to look for him.</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="6847">xás</m>
					</w>
					<w gl="he saw">
						<m id="6175">u</m>
						<m id="4155">má</m>
					</w>
					"<w>
						<m id="3670">káan</m>
					</w>
					<w gl="on the edge of the river">
						<m id="3007">ishkéesh</m>
						<m id="6046">tiim</m>
					</w>
				<w gl="they were there">
						<m id="3977">kun</m>
						<m id="1664">'iin</m>
					</w>."
				</kar>
				<eng>And he saw that they were there on the edge of the river.</eng>
			</s>
			<s>
				<kar>
					<w gl="he took him home downriverward">
						<m id="6175">u</m>
						<m id="4951">pípas</m>
						<m id="5193">rup</m>
					</w>
					<w gl="his child">
						<m id="4407">pa</m>
						<m id="4260">mu</m>
						<m id="511">'aramah</m>
					</w>.
				</kar>
				<eng>He took his child back home.</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="6847">xás</m>
					</w>
					<w gl="he said">
						<m id="6175">u</m>
						<m id="2733">pêer</m>
					</w>
					"<w>
						<m id="6881">xáyfaat</m>
					</w>
					<w>
						<m id="1729">ík</m>
					</w>
					<w>
						<m id="6675">vúra</m>
					</w>
					<w>
						<m id="1377">haríxay</m>
					</w>
					<w>
						<m id="3669">kâam</m>
					</w>
					<w>
						<m id="4076">kúuk</m>
					</w>
					<w gl="you go back">
						<m id="1418">i</m>
						<m id="1692">'íipma</m>
					</w>.
				</kar>
				<eng>And he told him, "You mustn't ever go back upriver there.</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="2517">imtarásuun</m>
					</w>
					<w gl="the young woman">
						<m id="4407">pa</m>
						<m id="1459">'ifápiit</m>
					</w>.
				</kar>
				<eng>The girl is a bastard.</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="5104">púra fâat</m>
					</w>
					<w gl="they did not pay to legitimize her">
						<m id="1476">ifkírahi</m>
						<m id="6035">tih</m>
						<m id="1371">ara</m>
					</w>."
				</kar>
				<eng>Nothing was paid to legitimize her."</eng>
			</s>
		</paragraph>
		<paragraph>
			<s>
				<kar>
					<w>
						<m id="2391">imáan</m>
					</w>
					<w gl="she swam across-river again">
						<m id="6175">u</m>
						<m id="2674">p</m>
						<m id="1917">íkpuuhkar</m>
					</w>
					<w gl="the young woman">
						<m id="4407">pa</m>
						<m id="1459">'ifápiit</m>
					</w>.
				</kar>
				<eng>The next day the girl swam across again.</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="1396">hôoy</m>
					</w>
					<w>
						<m id="3809">kích</m>
					</w>
					<w>
						<m id="4039">kusrípan</m>
					</w>.
				</kar>
				<eng>Where was Madrone?</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="6780">xanahíchyav</m>
					</w>
					<w>
						<m id="6675">vúra</m>
					</w>
					<w gl="she had">
						<m id="5506">t</m>
						<m id="6175">óo</m>
					</w>
					<w>
						<m id="1974">kríi</m>
					</w>.
				</kar>
				<eng>She stayed quite a long time.</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="7212">yúruk</m>
					</w>
					<w gl="she was looking downriver">
						<m id="6175">u</m>
						<m id="3424">trûupu</m>
						<m id="6035">tih</m>
					</w>.
				</kar>
				<eng>She looked downriver.</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="7038">yánava</m>
					</w>
					<w gl="he did">
						<m id="5506">t</m>
						<m id="6175">óo</m>
					</w>
					<w>
						<m id="2204">kvíripraa</m>
					</w>.
				</kar>
				<eng>She saw him run upriverward.</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="6847">xás</m>
					</w>
					<w gl="he said">
						<m id="6175">u</m>
						<m id="4823">píip</m>
					</w>
					"<w>
						<m id="1102">chéemyaach</m>
					</w>
					<w gl="let's swim back across">
						<m id="4370">nu</m>
						<m id="2674">p</m>
						<m id="1917">íkpuuhkar</m>
						<m id="1419">i</m>
					</w>.
				</kar>
				<eng>And he said, "Let's swim across from them quickly!</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="2671">íp</m>
					</w>
					<w gl="they told me">
						<m id="3738">kan</m>
						<m id="2733">éepeer</m>
						<m id="738">at</m>
					</w>
					'<w>
						<m id="6881">xáyfaat</m>
					</w>
					<w>
						<m id="4385">ôok</m>
					</w>
					<w gl="you go upriver">
						<m id="1418">i</m>
						<m id="2633">naa</m>
					</w>'."
				</kar>
				<eng>They told me, 'Don't go up there.'"</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="6847">xás</m>
					</w>
					<w gl="they swam across">
						<m id="3977">kun</m>
						<m id="3277">íthpuuhrin</m>
					</w>.
				</kar>
				<eng>So they swam across.</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="6847">xás</m>
					</w>
					<w>
						<m id="4213">máruk</m>
					</w>
					<w gl="they ran uphill">
						<m id="3977">kun</m>
						<m id="3314">ithvíripuraa</m>
					</w>.
				</kar>
				<eng>And they ran uphill.</eng>
			</s>
			<s>
				<kar>
					<w gl="the young woman">
						<m id="4407">pa</m>
						<m id="1459">'ifápiit</m>
					</w>
					<w gl="her house">
						<m id="4260">mu</m>
						<m id="2032">krívraam</m>
					</w>
					<w>
						<m id="4076">kúuk</m>
					</w>
					<w gl="he arrived">
						<m id="6175">u</m>
						<m id="6306">'uum</m>
					</w>.
				</kar>
				<eng>They got to the girl's house.</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="7006">yáan</m>
					</w>
					<w>
						<m id="6675">vúra</m>
					</w>
					<w gl="it was becoming evening">
						<m id="6175">ú</m>
						<m id="2298">kxurarahi</m>
						<m id="6035">tih</m>
					</w>.
				</kar>
				<eng>It was just evening.</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="3343">ithyáruk</m>
					</w>
					<w gl="there was talking">
						<m id="6175">u</m>
						<m id="6304">'uhyan</m>
						<m id="503">ára</m>
						<m id="1356">hi</m>
						<m id="6035">tih</m>
					</w>.
				</kar>
				<eng>There was talking across-stream.</eng>
			</s>
			<s>
				<kar>
					<w gl="he said">
						<m id="6175">u</m>
						<m id="4823">píip</m>
					</w>
					"<w>
						<m id="4299">naa</m>
					</w>
					<w gl="I think">
						<m id="4349">ni</m>
						<m id="6939">xú</m>
						<m id="6035">tih</m>
					</w>
					<w>
						<m id="5506">tá</m>
					</w>
					<w gl="they are coming here to search for me">
						<m id="3738">kana</m>
						<m id="4545">pápivar</m>
						<m id="6216">uk</m>
					</w>.
				</kar>
				<eng>He said, "I think they're coming to look for me."</eng>
			</s>
		</paragraph>
		<paragraph>
			<s>
				<kar>
					<w>
						<m id="6847">xás</m>
					</w>
					<w gl="they ran">
						<m id="3977">kun</m>
						<m id="3305">íthvip</m>
					</w>.
				</kar>
				<eng>Then they ran.</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="3241">ithéekxaram</m>
					</w>
					<w>
						<m id="6675">vúra</m>
					</w>
					<w>
						<m id="1397">hôoyva</m>
					</w>
					<w gl="they spent the night">
						<m id="3977">kun</m>
						<m id="2186">ikvéesh</m>
					</w>.
				</kar>
				<eng>They spent the whole night somewhere.</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="2392">imáankam</m>
					</w>
					<w gl="they came back down">
						<m id="3977">kun</m>
						<m id="2674">p</m>
						<m id="2932">irúviishrih</m>
					</w>.
				</kar>
				<eng>The next day they came back down.</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="6682">vúrava</m>
					</w>
					<w>
						<m id="3343">ithyáruk</m>
					</w>
					<w gl="there was talking">
						<m id="6175">u</m>
						<m id="6304">'uhyan</m>
						<m id="503">ára</m>
						<m id="1356">hi</m>
						<m id="6035">tih</m>
					</w>.
				</kar>
				<eng>There was talking across-stream.</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="6847">xás</m>
					</w>
					<w>
						<m id="4039">kusrípan</m>
					</w>
					<w gl="he said">
						<m id="6175">u</m>
						<m id="4823">píip</m>
					</w>
					"<w>
						<m id="1144">chími</m>
					</w>
					<w gl="let me go back home">
						<m id="3735">kan</m>
						<m id="2860">ipvâaram</m>
						<m id="1419">i</m>
					</w>.
				</kar>
				<eng>And Madrone said, "Let me go back home!</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="4246">miník</m>
					</w>
					<w gl="I will return">
						<m id="4349">ni</m>
						<m id="2694">'ipak</m>
						<m id="1379">eesh</m>
					</w>."
				</kar>
				<eng>I'll return, all right."</eng>
			</s>
			<s>
				<kar>
					<w gl="he swam back">
						<m id="6175">u</m>
						<m id="2674">p</m>
						<m id="1919">íkpuuhkin</m>
					</w>.
				</kar>
				<eng>He swam back across.</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="6847">xás</m>
					</w>
					<w gl="he was looking at it">
						<m id="6175">ú</m>
						<m id="2585">muusti</m>
					</w>
					<w gl="his body">
						<m id="4407">pa</m>
						<m id="4260">mu</m>
						<m id="1696">'iish</m>
					</w>.
				</kar>
				<eng>Then he looked at his body.</eng>
			</s>
			<s>
				<kar>
					<w gl="it did">
						<m id="5506">t</m>
						<m id="6175">óo</m>
					</w>
					<w gl="scale off">
						<m id="2604">mxaxasúr</m>
						<m id="6380">oo</m>
					</w>,
					<w gl="it peeled">
						<m id="5506">t</m>
						<m id="6175">u</m>
						<m id="6253">'ur</m>
					</w>.
				</kar>
				<eng>It was scaling off, it was peeling.</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="4039">kusrípan</m>
					</w>
					<w gl="he turned into it">
						<m id="6175">u</m>
						<m id="2773">pkêevish</m>
					</w>.
				</kar>
				<eng>He turned into a madrone tree.</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="6383">vaa</m>
					</w>
					<w>
						<m id="6675">vúra</m>
					</w>
					<w>
						<m id="4716">payêem</m>
					</w>
					<w gl="you will see it">
						<m id="3939">ku</m>
						<m id="4155">máh</m>
						<m id="1379">eesh</m>
					</w>,
					<w gl="it is peeling">
						<m id="6175">u</m>
						<m id="6253">'úun</m>
						<m id="6035">tih</m>
					</w>.
				</kar>
				<eng>You will see it that way now, it is peeling.</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="82">ayu'âach</m>
					</w>
					<w gl="the young woman">
						<m id="4407">pa</m>
						<m id="1459">'ifápiit</m>
					</w>
					<w>
						<m id="5506">tée</m>
					</w>
					<w>
						<m id="2671">p</m>
					</w>
					<w>
						<m id="6756">xákaan</m>
					</w>
					<w gl="they spent the night">
						<m id="3977">kun</m>
						<m id="2186">ikvéeshrih</m>
						<m id="738">at</m>
					</w>.
				</kar>
				<eng>It is because he spent the night with the girl.</eng>
			</s>
			<s>
				<kar>
					<w>
						<m id="6570">víri</m>
					</w>
					<w>
						<m id="6383">vaa</m>
					</w>
					<w>
						<m id="6675">vúra</m>
					</w>
					<w gl="still">
						<m id="3970">kuma</m>
						<m id="3745">kári</m>
					</w>
					<w gl="you will see him">
						<m id="3939">ku</m>
						<m id="4155">máh</m>
						<m id="1379">eesh</m>
					</w>
					<w>
						<m id="4039">kusrípan</m>
					</w>,
					<w>
						<m id="3382">itíhaan</m>
					</w>
					<w gl="its year">
						<m id="3970">kuma</m>
						<m id="1373">hárinay</m>
					</w>
					<w gl="he peels">
						<m id="5506">t</m>
						<m id="6175">u</m>
						<m id="6253">'ur</m>
					</w>.
				</kar>
				<eng>You will see him that way still, every year he peels.</eng>
			</s>
		</paragraph>
	</data>
</text>