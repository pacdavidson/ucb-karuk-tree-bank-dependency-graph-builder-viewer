<?xml version="1.0" encoding="UTF-8"?>
   <text id="WB_KL-55">
		<metadata>
			<title>"Wrestling Medicine"</title>
			<author>Mamie <alph>Offield</alph></author>
			<date>1957</date>
			<publication>William Bright, <em>The Karok Language</em> (1957), pp. 260-261, Text 55</publication>
		</metadata>
		<data>
			<paragraph>
				<s>
					<kar>
						<w>
							<m id="3207">itáharavan</m>
						</w>
						<w gl="they grew up">
							<m id="3977">kun</m>
							<m id="1640">'íifshipree</m>
							<m id="1369">nik</m>
						</w>
						<w gl="brothers">
							<m id="6081">tipahêer</m>
							<m id="584">as</m>
						</w>.
					</kar>
					<eng>Ten brothers once grew up.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w gl="edge of the fireplace">
							<m id="197">ahinám</m>
							<m id="6047">tiich</m>
						</w>
						<w>
							<m id="6675">vúra</m>
						</w>
						<w gl="he sat">
							<m id="6175">u</m>
							<m id="1974">krêe</m>
							<m id="1369">nik</m>
						</w>
						<w>
							<m id="353">amtápnihich</m>
						</w>.
					</kar>
					<eng>And (one of them, named kunâach'aa) sat at the edge of the fireplace, in the ashes.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w gl="they said">
							<m id="3977">kun</m>
							<m id="4823">piip</m>
						</w>, "
						<w>
							<m id="4215">maruk'áraar</m>
						</w>
						<w>
							<m id="6383">vaa</m>
						</w>
						<w>
							<m id="3670">káan</m>
						</w>
						<w gl="edge of the lake">
							<m id="6222">úknam</m>
							<m id="6047">tiimich</m>
						</w>
						<w gl="he is staying">
							<m id="6175">ú</m>
							<m id="1974">krii</m>
						</w>.
					</kar>
					<eng>And they said, "A giant is staying there at the edge of the lake.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="6570">víri</m>
						</w>
						<w>
							<m id="234">akâay</m>
						</w>
						<w>
							<m id="6675">vúra</m>
						</w>
						<w>
							<m id="6757">xákaan</m>
						</w>
						<w gl="they will wrestle">
							<m id="3977">kun</m>
							<m id="6710">vúunv</m>
							<m id="1379">eesh</m>
						</w>."
					</kar>
					<eng>Who will wrestle with him?"</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="6174">ta'ítam</m>
						</w>
						<w gl="he thought">
							<m id="6175">u</m>
							<m id="6939">xús</m>
							<m id="1369">anik</m>
						</w>
						<w gl="the strongest one">
							<m id="4407">p</m>
							<m id="1905">eekpihan</m>
							<m id="5522">tâapas</m>
						</w>, "
						<w>
							<m id="6042">tîi</m>
						</w>
						<w>
							<m id="6757">xákaan</m>
						</w>
						<w gl="let us wrestle!">
							<m id="4370">nú</m>
							<m id="6710">vuunv</m>
							<m id="1419">i</m>
						</w>."
					</kar>
					<eng>So the strongest one thought, "Let me wrestle with him!"</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w gl="when he went">
							<m id="4408">p</m>
							<m id="6175">oo</m>
							<m id="6396">vâaram</m>
						</w>,
						<w gl="towards uphill a little">
							<m id="4147">mâam</m>
							<m id="6444">vanih</m>
							<m id="1420">ich</m>
						</w>
						<w gl="he had">
							<m id="5506">t</m>
							<m id="6175">óo</m>
						</w>
						<w>
							<m id="1786">kfuukra</m>
						</w>.
					</kar>
					<eng>So when he went, he climbed a little ways uphill.</eng>
				</s>
			</paragraph>
			<paragraph>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w gl="he rushed outdoors">
							<m id="6175">u</m>
							<m id="569">'árihrupuk</m>
						</w>,
						<w>
							<m id="3980">kunâach'aa</m>
						</w>.
					</kar>
					<eng>Then kunâach'aa went outdoors.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w gl="a palmful">
							<m id="809">átruup</m>
							<m id="1011">'axyar</m>
						</w>
						<w>
							<m id="346">ámtaap</m>
						</w>
						<w gl="he carried it outdoors in his hand">
							<m id="6175">u</m>
							<m id="4283">mûut</m>
							<m id="5199">rupuk</m>
						</w>.
					</kar>
					<eng>And he brought a handful of ashes outdoors.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w gl="he shouted">
							<m id="6175">u</m>
							<m id="1902">kpêehva</m>
						</w>.
					</kar>
					<eng>And he shouted.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w>
							<m id="6383">vaa</m>
						</w>
						<w gl="he thought">
							<m id="6175">u</m>
							<m id="4823">piip</m>
						</w>, "
						<w>
							<m id="1660">iimkun</m>
						</w>
						<w>
							<m id="1402">úm</m>
						</w>
						<w>
							<m id="4059">kúth</m>
						</w>
						<w gl="I am growing up">
							<m id="4349">ni</m>
							<m id="1453">'íif</m>
							<m id="6035">tih</m>
						</w>,
						<w>
							<m id="1660">iimkun</m>
						</w>
						<w>
							<m id="1402">úm</m>
						</w>
						<w>
							<m id="3971">kumá'ii</m>
						</w>
						<w gl="I am growing up">	
							<m id="4349">ni</m>
							<m id="1453">'íif</m>
							<m id="6035">tih</m>
						</w>.
					</kar>
					<eng>And he said, "Am I growing up for you people, am I growing up for your sake?</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="4299">naa</m>
						</w>
						<w>
							<m id="3752">káru</m>
						</w>
						<w>
							<m id="6675">vúra</m>
						</w>
						<w>
							<m id="3670">káan</m>
						</w>
						<w gl="I am growing up">
							<m id="4349">ni</m>
							<m id="1453">'íif</m>
							<m id="6035">tih</m>
						</w>."
					</kar>
					<eng>I am growing up here too!"</eng>
				</s>
			</paragraph>
			<paragraph>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w gl="he heard">
							<m id="6175">u</m>
							<m id="5954">thítiv</m>
						</w>,
						<w gl="visibly it">
							<m id="7037">y</m>
							<m id="6175">óo</m>
						</w>
						<w>
							<m id="1438">chrívchav</m>
						</w>
						<w gl="the water">
							<m id="4407">pá</m>
							<m id="50">'aas</m>
						</w>,
						<w gl="in the lake">
							<m id="4407">pa</m>
							<m id="6222">'úkraam</m>
						</w>,
						<w gl="when they threw him into the water">
							<m id="4408">pa</m>
							<m id="3977">kun</m>
							<m id="4429">páathkuri</m>
						</w>
						<w gl="his brother">
							<m id="4407">pa</m>
							<m id="4260">mu</m>
							<m id="6081">típah</m>
						</w>.
					</kar>
					<eng>Then he heard it, he saw the water splash in the lake, when (the giant) threw his brother in.</eng>
					<note>Use of kun- in this sentence and the following I think is for literary effect, to make it seem like there are many antagonists or to obscure him. I have glossed the subject as "they" to try and preserve this.</note>
				</s>
				<s>
					<kar>
						<w>
							<m id="1096">chavúra</m>
						</w>
						<w>
							<m id="3930">koovúra</m>
						</w>
						<w>
							<m id="5506">tá</m>
						</w>
						<w gl="they threw them into the water">
							<m id="3977">kun</m>
							<m id="3551">ixyákurih</m>
						</w>.
					</kar>
					<eng>Finally (the giant) threw all (the brothers) in.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3930">koovúra</m>
						</w>
						<w>
							<m id="6383">vaa</m>
						</w>
						<w gl="he was saying">
							<m id="6175">u</m>
							<m id="4823">pí</m>
							<m id="6035">tih</m>
						</w>, "
						<w>
							<m id="1660">iimkun</m>
						</w>
						<w>
							<m id="1402">úm</m>
						</w>
						<w>
							<m id="4059">kúth</m>
						</w>
						<w gl="I am growing up">
							<m id="4349">ni</m>
							<m id="1453">'íif</m>
							<m id="6035">tih</m>
						</w>.
					</kar>
					<eng>Every time, (kunâach'aa) said, "Am I growing up for you people?</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="4299">naa</m>
						</w>
						<w>
							<m id="3752">káru</m>
						</w>
						<w>
							<m id="6675">vúra</m>
						</w>
						<w>
							<m id="3670">káan</m>
						</w>
						<w gl="I am growing up">
							<m id="4349">ni</m>
							<m id="1453">'íif</m>
							<m id="6035">tih</m>
						</w>,
						<w>
							<m id="3980">kunâach'aa</m>
						</w>."
					</kar>
					<eng>I am growing up here too, (I,) kunâach'aa!"</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w gl="he thought">
							<m id="6175">u</m>
							<m id="6939">xus</m>
						</w>, "
						<w>
							<m id="1410">hûut</m>
						</w>
						<w gl="I will do it">
							<m id="4349">ni</m>
							<m id="4086">kuuph</m>
							<m id="1379">eesh</m>
						</w>.
					</kar>
					<eng>And he thought, "What shall I do?</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="4450">pácheech</m>
						</w>
						<w>
							<m id="5506">tá</m>
						</w>
						<w gl="I remain">
							<m id="4349">ni</m>
							<m id="5206">saam</m>
						</w>.
					</kar>
					<eng>I am left alone.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="1144">chími</m>
						</w>
						<w gl="let me go!">
							<m id="3735">kan</m>
							<m id="6396">vâaram</m>
							<m id="1419">i</m>
						</w>
						<w gl="I myself">
							<m id="4299">ná</m>
							<m id="4259">mpaan</m>
						</w>.
					</kar>
					<eng>Let <em>me</em> go (to wrestle)!"</eng>
					<note>Bright underlines "me" in the English translation</note>
				</s>
				<s>
					<kar>
						<w>
							<m id="6174">ta'ítam</m>
						</w>
						<w gl="he went">
							<m id="6175">u</m>
							<m id="6396">vâaram</m>
							<m id="1378">aheen</m>
						</w>.
					</kar>
					<eng>So he left.</eng>
					<note>Bright does not offer an English translation for this sentence. Sentence 19 of his English translation corresponds to sentence 20 of the Karuk. The English of sentence 19 seems to not fit either Karuk 19 or 20 very well, but seems to be a mix of the two.</note>
				</s>
			</paragraph>
			<paragraph>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w>
							<m id="6383">vaa</m>
						</w>
						<w>
							<m id="3670">káan</m>
						</w>
						<w gl="he arrived">
							<m id="6175">u</m>
							<m id="6306">'uum</m>
						</w>.
					</kar>
					<eng>So he went there.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="6174">ta'ítam</m>
						</w>
						<w gl="he laughed">
							<m id="6175">u</m>
							<m id="2079">ksáh</m>
							<m id="1378">een</m>
						</w>
						<w gl="the giant">
							<m id="4407">pa</m>
							<m id="4215">maruk'áraar</m>
						</w>,
						<w gl="he thought">
							<m id="6175">u</m>
							<m id="6939">xus</m>
						</w>, "
						<w>
							<m id="1457">íf</m>
						</w>
						<w>
							<m id="4352">nîinamich</m>
						</w>
						<w gl="for together">
							<m id="4408">pa</m>
							<m id="6756">xákaan</m>
						</w>
						<w gl="we are going to wrestle">
							<m id="4370">nu</m>
							<m id="6710">vúunv</m>
							<m id="1379">eesh</m>
						</w>."
					</kar>
					<eng>And the giant laughed, he thought, "He's really small for us to wrestle together!"</eng>
				</s>
			</paragraph>
			<paragraph>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w>
							<m id="6174">ta'ítam</m>
						</w>
						<w gl="they wrestled">
							<m id="3977">kun</m>
							<m id="6710">vúunv</m>
							<m id="1378">aheen</m>
						</w>.
					</kar>
					<eng>So then they wrestled.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w>
							<m id="3930">koovúra</m>
						</w>
						<w gl="the little plants">
							<m id="4407">pa</m>
							<m id="4963">pinish</m>
							<m id="6128">tunvêech</m>
							<m id="584">as</m>
						</w>
						<w>
							<m id="3752">káru</m>
						</w>
						<w>
							<m id="3930">koovúra</m>
						</w>
						<w gl="the trees">
							<m id="4407">pa</m>
							<m id="2682">'ípaha</m>
						</w>
						<w gl="they were shouting to him">
							<m id="3977">kun</m>
							<m id="1632">ihyûunish</m>
							<m id="6035">tih</m>
						</w>, "
						<w>
							<m id="3980">kunâach'aa</m>
						</w>, 
						<w gl="go hard!">
							<m id="5156">puxîich</m>
							<m id="1419">i</m>
						</w>."
					</kar>
					<eng>And all the little plants and all the trees shouted to him, "Go to it, kunâach'aa!"</eng>
				</s>
			</paragraph>
			<paragraph>
				<s>
					<kar>
						<w>
							<m id="3745">kári</m>
						</w>
						<w>
							<m id="6847">xás</m>
						</w>
						<w gl="he threw him down">
							<m id="6175">u</m>
							<m id="4452">páchish</m>
						</w>
						<w gl="the giant">
							<m id="4407">pa</m>
							<m id="4215">maruk'áraar</m>
						</w>,
						<w>
							<m id="6222">ukráam</m>
						</w>
						<w gl="he threw him into water">
							<m id="6175">u</m>
							<m id="4429">paathkúrih</m>
						</w>.
					</kar>
					<eng>Then he threw the giant down, he threw him in the lake.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="6174">ta'ítam</m>
						</w>
						<w>
							<m id="3930">koovúra</m>
						</w>
						<w gl="they came back to life">
							<m id="3977">kun</m>
							<m id="4925">pimtáv</m>
							<m id="1378">aheen</m>
						</w>
						<w gl="his brothers">
							<m id="4407">pa</m>
							<m id="4260">mu</m>
							<m id="6081">tipáh</m>
							<m id="1724">iivshas</m>
						</w>.
					</kar>
					<eng>And all his brothers came back to life.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="6174">ta'ítam</m>
						</w>
						<w gl="they went back home">
							<m id="3977">kun</m>
							<m id="4695">pávyiihshipr</m>
							<m id="1378">een</m>
						</w>.
					</kar>
					<eng>So they went back home.</eng>
				</s>
				<s>
					<kar>
						<w>
							<m id="5163">púyava</m>
						</w>
						<w>
							<m id="6383">vaa</m>
						</w>
						<w gl="he did it">
							<m id="6175">u</m>
							<m id="4086">kúphaa</m>
							<m id="1369">nik</m>
						</w>
						<w>
							<m id="3980">kunâach'aa</m>
						</w>.
					</kar>
					<eng>kunâach'aa did that.</eng>
				</s>
			</paragraph>
		</data>
	</text>
							
						
						
							
						
							
						
							
					
							
						
							
							
						
						
							
							
						
				
						
						 
							
							
							
							
						
							
						
						
						
						
				